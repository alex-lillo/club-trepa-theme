<?php
/**
 * Template Name: Static Template w/ Sidebar
 * Description: A Page Template that adds a different sidebar to pages
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">

				<?php the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php comments_template( '', true ); ?>

			</div><!-- #content -->
		</div><!-- #primary -->

<?php if ( !function_exists('dynamic_sidebar')
|| !dynamic_sidebar('sidebar2') ) : ?>
<?php endif; ?>
<?php get_footer(); ?>